# ---- Base Node ----
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost1.55-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/cannabiscoindev/cannabiscoin420.git /opt/cannabiscoin && \
    cd /opt/cannabiscoin/src && \
    make -f makefile.unix

# ---- Release ----
FROM ubuntu:14.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost1.55-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r cannabiscoin && useradd -r -m -g cannabiscoin cannabiscoin
RUN mkdir /data
RUN chown cannabiscoin:cannabiscoin /data
COPY --from=build /opt/cannabiscoin/src/CannabisCoind /usr/local/bin/
USER cannabiscoin
VOLUME /data
EXPOSE 39347 39348
CMD ["/usr/local/bin/CannabisCoind", "-datadir=/data", "-conf=/data/CannabisCoin.conf", "-server", "-txindex", "-printtoconsole"]